<?php
!isset($_GET['cl_id']) ? die() : true;

require_once("../../../../wp-load.php");
require __DIR__.'/../inc/fpdf/fpdf.php';

$p_id = $_GET['cl_id'];


$business = get_field('business',$p_id);
$client = get_field('client',$p_id);
$b_id = $business->ID;
$c_id = $client->ID;

$space = html_entity_decode('&#09;&#09;&#09;');
$gbp = utf8_decode('£');
$aud = utf8_decode('$');

$pdf = new FPDF('L');
$pdf->AddPage();
$pdf->SetDrawColor(200,200,200);



// Logo
$logo_img = get_field('logo',$b_id)['url'];
$logo_size = get_field('logo_size',$b_id);
if($logo_img){
	$pdf->Image($logo_img,10,17,$logo_size);
}


// INVOICE

$pdf->SetTextColor(200,200,200);
$pdf->SetFont('Arial','B',!$logo_img ? 40 : 28);
if(!$logo_img){$pdf->setY(17);}
$pdf->Cell(100,!$logo_img ? 20 : 54,'INVOICE');
$pdf->Ln();


// Tables

// Invoice Data

// ID
$date = get_the_date('ymd',$p_id);
$id_number = $date.'-'.$p_id;

$pdf->SetFont('Arial','',12);
$pdf->SetTextColor(150,150,160);
$pdf->setY(50);
$pdf->Cell(30,7,'Invoice ID','R',0,'L',0);
$pdf->Cell(4,7,'',0,0,'L',0);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(50,7,$id_number,0,0,'L',0);
$pdf->Ln();


// Due date
$due_date = get_the_date('d/m/Y',$p_id);

$pdf->SetTextColor(150,150,160);
$pdf->Cell(30,7,'Issue Date','R',0,'L',0);
$pdf->Cell(4,7,'',0,0,'L',0);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(50,7,$due_date,0,0,'L',0);
$pdf->Ln();


// Issue date
$due_date = get_the_date('Y/m/d',$p_id);
$due_date = date('d/m/Y', strtotime("$due_date +2 months"));

$pdf->SetTextColor(150,150,160);
$pdf->Cell(30,7,'Due Date','R',0,'L',0);
$pdf->Cell(4,7,'',0,0,'L',0);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(50,7,$due_date,0,0,'L',0);



// From
$from_title = get_field('business_name',$b_id);
$from_contact = get_field('contact_details',$b_id);

$pdf->SetXY(100,18);
$pdf->SetTextColor(150,150,160);
$pdf->Cell(32,8,'From'.$space,0,0,'R',0);

$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(60,8,$from_title,'R',2,'L',0);

$pdf->SetFont('Arial','',11);
$pdf->MultiCell(60,5,$from_contact,'R',2,0);


// For
$for_title = get_field('company_name',$c_id);
$for_contact = get_field('contact_details',$c_id);

$pdf->Ln();
$pdf->SetXY(180,18);

$pdf->SetTextColor(150,150,160);
$pdf->Cell(32,8,'For'.$space,0,0,'R',0);

$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(100,100,100);
$pdf->Cell(50,8,$for_title,0,2,'L',0);

$pdf->SetFont('Arial','',11);
$pdf->MultiCell(60,5,$for_contact,0,2,0);


// Table Items

// Header
$pdf->SetXY(10,80);
$pdf->SetFont('Arial','B',11);
$line_height = 8;

$pdf->setFillColor(150,214,255);
$pdf->SetTextColor(80,80,80);
$pdf->Cell(30,$line_height,$space.'Item Type',0,0,'L',1);
$pdf->Cell(144,$line_height,$space.'Description',0,0,'L',1);
$pdf->Cell(30,$line_height,'Qty.'.$space,0,0,'R',1);
$pdf->Cell(35,$line_height,'Unit Price'.$space,0,0,'R',1);
$pdf->Cell(35,$line_height,'Amount'.$space,0,0,'R',1);
$pdf->Ln();


$currency = get_field('currency',$p_id) === 'aud' ? $aud : $gbp;

$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(100,100,100);
$pdf->SetFont('Arial','',11);
if(have_rows('items',$p_id)):$total = 0;
	while(have_rows('items',$p_id)):the_row();
		$item_type = get_sub_field('item_type');
		$description = mb_strimwidth(get_sub_field('description'), 0, 72);
		$quantity = get_sub_field('quantity');
		$unit_price = get_sub_field('unit_price');
		$amount = get_sub_field('amount');
		$pdf->Cell(30,$line_height,$space.$item_type,'LBR',0,'L',0);
		$pdf->Cell(144,$line_height,$space.$description,'BR',0,'L',0);
		$pdf->Cell(30,$line_height,$quantity.$space,'BR',0,'R',0);
		$pdf->Cell(35,$line_height,$currency.$unit_price.$space,'BR',0,'R',0);
		$pdf->Cell(35,$line_height,$currency.$amount.$space,'BR',0,'R',0);
		$pdf->Ln();
		$total += (int)$amount;
	endwhile;
endif;

$pdf->SetFont('Arial','',14);
$pdf->Cell(239,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(204,$line_height,'',0,0,'L',0);
$pdf->Cell(35,$line_height,'Total',0,0,'R',0);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(35,$line_height,$currency.$total,0,0,'R',0);
$pdf->Ln();
$pdf->Ln();


// Payment Methods

$pd_count = count(get_field('payment_options',$b_id));
if(have_rows('payment_options',$b_id)):

	$pdf->setFillColor(244,244,244);
	$pdf->SetFont('Arial','B',12);
	$payment_title = $pd_count > 1 ? 'Payment Methods:' : 'Payable To:';
	$pdf->Cell(35,$line_height,$payment_title,0,0,'L',0);

	$pdf->Ln();
	$pdf->SetFont('Arial','',12);
	$y = $pdf->GetY();
	$x = 10;
	$w = 60;
	$j = 10;
	$i = 0;
	while(have_rows('payment_options',$b_id)):the_row();

		if($i){
			$pdf->SetXY($x+($w*$i)+($j*($i-1)),$y+2);
			$pdf->MultiCell($j,10,'or',0,'C');
		}
		$details = get_sub_field('details');
		$pdf->SetXY($x+($w*$i)+($j*$i),$y+2);
		$pdf->MultiCell($w,7,$details,0,'L',1);
		$i++;
	endwhile;
endif;

$notes = get_field('invoice_notes',$p_id);
if($notes){
	$pdf->SetXY($x+($w*$i)+($j*$i),$y+2);
	$pdf->MultiCell(134,7,"Note: \n".$notes,1,'L',0);
}

// Output
$date = get_the_date('Y',$p_id);
$pdf->Output('INV-'.$date.$p_id.'.pdf','I');
