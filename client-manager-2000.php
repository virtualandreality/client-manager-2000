<?php
/*
Plugin Name: Client Manager 2000
Plugin URI: http://www.virtualandreality.com
Description: WP Plugin to manage client details and create invoice PDFs
Author: Kyle Skinner
Version: 1.0
Author URI: http://www.virtualandreality.com
Depends: ACF Pro
*/

class ClientManager2000{

	function __construct(){
		add_action('init',[$this,'register_scripts'], 10, 1);
		add_action( 'wp_enqueue_scripts',[$this,'register_styles']);
		add_action( 'init',[$this,'register_acf_options_page']);
		add_action( 'admin_enqueue_scripts',[$this,'register_admin_styles']);
		add_action('init',[$this,'register_custom_post_types'], 10, 1);
		add_action('edit_form_top',[$this,'single_client_post_header'], 10, 1);
	}

	// setup

		function register_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('cm2000-script',plugins_url('assets/script.js',__FILE__));
		}

		function register_styles(){
			wp_enqueue_style('cm2000-css',plugins_url('assets/style.css',__FILE__));
		}

		function register_admin_styles(){
			wp_enqueue_style('cm2000-admin-css',plugins_url('assets/style-admin.css',__FILE__));
		}

		function register_custom_post_types(){

			register_post_type('cm2k_businesses',[
				'public'    => true,
				'label'     => __('Businesses','textdomain'),
				'menu_icon' => 'dashicons-universal-access',
			]);

			register_post_type('cm2k_clients',[
			    'public'    => true,
			    'label'     => __('Clients','textdomain'),
			    'menu_icon' => 'dashicons-groups',
			]);

			register_post_type('cm2k_invoices',[
				'public'    => true,
				'label'     => __('Invoices','textdomain'),
				'menu_icon' => 'dashicons-clipboard',
			]);
		}

		function register_acf_options_page(){

			if(function_exists('acf_add_options_page')){
				acf_add_options_page(array(
					'page_title' 	=> 'Your Settings',
					'menu_title'	=> 'Your Settings',
					'menu_slug' 	=> 'cm2k-your-settings',
					'capability'	=> 'edit_posts',
					'redirect'		=> false
				));
			}
		}

		function single_client_post_header(){

			if(get_post_type() === 'cm2k_invoices'){
				include __DIR__.'/template-parts/single-client-post-header.php';
			}
		}

}
$ClientManager2000 = new ClientManager2000();